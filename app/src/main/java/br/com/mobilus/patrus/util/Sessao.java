package br.com.mobilus.patrus.util;

import com.google.gson.annotations.SerializedName;

import br.com.mobilus.patrus.model.Cliente;
import br.com.mobilus.patrus.model.Encomenda;

public class Sessao {


    public static final int FLAG_DETALHE_PEDIDO = 0;
    public static final int FLAG_DETALHE_HISTORICO = 1;
    public static int flagDetalhe = FLAG_DETALHE_PEDIDO;

    @SerializedName("Cliente")
    private Cliente cliente;

    @SerializedName("Encomenda")
    private Encomenda encomenda;

    public Encomenda getEncomenda() {
        return encomenda;
    }

    public void setEncomenda(Encomenda encomenda) {
        this.encomenda = encomenda;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}