package br.com.mobilus.patrus.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import br.com.mobilus.droidlib.service.ErrosV1;
import br.com.mobilus.droidlib.service.ServiceAdapterV1_2;
import br.com.mobilus.droidlib.service.data.response.ResponseV1;
import br.com.mobilus.droidlib.service.retrofit.callback.CallbackV1_2;
import br.com.mobilus.droidlib.util.AlertasV1;
import br.com.mobilus.patrus.LoginActivity;
import br.com.mobilus.patrus.MainActivity;
import br.com.mobilus.patrus.R;
import br.com.mobilus.patrus.service.Post;
import br.com.mobilus.patrus.service.data.RequestContato;
import br.com.mobilus.patrus.util.PatrusUtil;
import br.com.mobilus.patrus.util.SharedPref;
import retrofit.client.Response;

public class ContatoFragment extends BaseFragmentV4 implements  Validator.ValidationListener{


	@NotEmpty(messageResId = R.string.campo_obrigatorio)
	EditText etNome;

	@Email(messageResId = R.string.email_invalido)
	@NotEmpty(messageResId = R.string.campo_obrigatorio)
	EditText etEmail;

	@NotEmpty(messageResId = R.string.campo_obrigatorio)
	EditText etMensagem;

	@NotEmpty(messageResId = R.string.campo_obrigatorio)
	EditText etAssunto;

	TextView tvHorario;

	Button btnEnviar;

	String hora;

	Validator validator;

	SharedPref sharedPref;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initialize(inflater, container, savedInstanceState, R.layout.fragment_contato);
		etNome = (EditText) rootView.findViewById(R.id.etNome);
		etEmail = (EditText) rootView.findViewById(R.id.etEmail);
		etMensagem = (EditText) rootView.findViewById(R.id.etMensagem);
		etAssunto = (EditText) rootView.findViewById(R.id.etAssunto);
		btnEnviar = (Button) rootView.findViewById(R.id.btnEnviar);

		((MainActivity) getActivity()).setToobarTitle("Fale Conosco");

        sharedPref = new SharedPref(getActivity());

		validator = new Validator(this);
		validator.setValidationListener(this);
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		btnEnviar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				validator.validate();
			}
		});
	}

	@Override
	public void onValidationSucceeded() {
		if (sharedPref.getUsuario() != null){
			servicoEmail();
		} else {
			Intent intent = new Intent(getActivity(), LoginActivity.class);
			startActivity(intent);
		}
	}

	private void servicoEmail() {

		RequestContato rc = new RequestContato();
		rc.setNome(etNome.getText().toString());
		rc.setEmail(etEmail.getText().toString());
		rc.setAssunto(etAssunto.getText().toString());
		rc.setMensagem(etMensagem.getText().toString());
        rc.setClienteID(sharedPref.getUsuario().getClienteId());

		CallbackV1_2 cbCadastrar = new CallbackV1_2<ResponseV1>(getContext(), R.string.aguarde, true)
		{
			@Override
			public void success(ResponseV1 responseV1, Response response) {
				super.success(responseV1, response);
				if(ErrosV1.NENHUM == validarResponseV1ComAlertas(getContext(), responseV1)){
					AlertasV1.alerta(getContext(), R.string.mensagem_enviada_com_sucesso).setOnDismissListener(new DialogInterface.OnDismissListener() {
						@Override
						public void onDismiss(DialogInterface dialog) {
							((MainActivity) getActivity()).refreshFragment();
						}
					});
				}
			}
		};

		ServiceAdapterV1_2.call(PatrusUtil.URL_SERVICE_BASE, Post.class).contato(rc, cbCadastrar);
	}

	@Override
	public void onValidationFailed(List<ValidationError> errors) {
		for (ValidationError error : errors) {
			View view = error.getView();
			String message = error.getCollatedErrorMessage(getActivity());
			if (view instanceof EditText) {
				((EditText) view).setError(message);
			}
		}
	}
}