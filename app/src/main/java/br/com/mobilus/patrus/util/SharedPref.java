package br.com.mobilus.patrus.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.google.gson.JsonParseException;

import java.io.IOException;

import br.com.mobilus.droidlib.util.DataSerializer;
import br.com.mobilus.patrus.model.Cliente;

public class SharedPref {


    private static SharedPref sharedPref;
    private SharedPreferences pref;
    private Editor editor;
    private Context _context;


    private static final String PREF_NAME = "patrus";
    public static final String KEY_SESSAO_SERIALIZED = "DADOSSESSAO";
    public static final String KEY_USUARIO = "USUARIO";
    public static final String KEY_EMAIL = "EMAIL";

    public SharedPref(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, Context.MODE_MULTI_PROCESS);
        editor = pref.edit();
    }

    public static SharedPref getInstance(Context context) {
        if (sharedPref == null)
            sharedPref = new SharedPref(context);

        return sharedPref;
    }

    public void logoutUser() {
        editor.clear();
        editor.commit();
    }

    public void saveSessionState(Sessao sessao) {
        try {
            Log.d("Patrus", "saveSessionState: " + DataSerializer.getInstance().toJson(sessao));
            editor.putString(KEY_SESSAO_SERIALIZED, DataSerializer.getInstance().toJson(sessao));
            if (sessao != null && sessao.getCliente() != null) {
                editor.putString(KEY_EMAIL, sessao.getCliente().getEmail());
            }
        } catch (IOException e) {
            editor.putString(KEY_SESSAO_SERIALIZED, "");
            e.printStackTrace();
        }
        editor.commit();
    }
    public void salveUsuario(Cliente token) throws IOException {
        editor.putString(KEY_USUARIO, DataSerializer.getInstance().toJson(token));
        editor.commit();
    }
    public Cliente getUsuario(){
        try {
            return DataSerializer.getInstance().toObject(pref.getString(KEY_USUARIO, null), Cliente.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
            return null;
        }
    }
    public Sessao restoreSessionState() {
        Sessao retorno = null;
        try {

            retorno = DataSerializer.getInstance().toObject(pref.getString(KEY_SESSAO_SERIALIZED, null), Sessao.class);
            if (retorno == null) {
                return new Sessao();
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Errooo", e.getMessage());
        }
        return retorno;
    }

    public String getEmail() {
        return pref.getString(KEY_EMAIL, "");
    }
}