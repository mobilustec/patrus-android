package br.com.mobilus.patrus;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import br.com.mobilus.droidlib.util.AlertasV1;
import br.com.mobilus.patrus.util.PatrusUtil;

public class EsqueciSenhaActivity extends ActivityBasePatrus {

    MaterialDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_esqueci_senha);

        View progress = getLayoutInflater().inflate(R.layout.progress_dialog_2_mobilus_lib, null);
        TextView tvCarregando = (TextView) progress.findViewById(R.id.tvProgressAndroidLib);
        tvCarregando.setText("carregando...");
        dialog = AlertasV1.alertaCustomView(this, progress, false);

        final WebView webview = (WebView) findViewById(R.id.webView);

        CookieManager.getInstance().setAcceptCookie(true);
        webview.setWebViewClient(new CustomWebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if(dialog != null){
                    try{
                        dialog.dismiss();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);


        webview.loadUrl(PatrusUtil.PROJECT_ESQUECI_SENHA_URL);
        webview.getSettings().setJavaScriptEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    //web view client implementation
    private class CustomWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            //do whatever you want with the url that is clicked inside the webview.
            //for example tell the webview to load that url.
            view.loadUrl(url);
            //return true if this method handled the link event
            //or false otherwise
            return true;
        }
    }
}