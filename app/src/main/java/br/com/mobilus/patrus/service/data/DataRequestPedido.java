package br.com.mobilus.patrus.service.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by samuel on 25/10/16.
 */

public class DataRequestPedido extends RequestBase {

    @SerializedName("TipoPessoa")
    private String tipoPessoa;
    @SerializedName("chave")
    private String chave;
    @SerializedName("CPF_CNPJ")
    private String cPFCNPJ;
    @SerializedName("Documento")
    private String documento;
    @SerializedName("TipoDocumento")
    private String tipoDocumento;

    public String getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(String tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    @Override
    public String getChave() {
        return chave;
    }

    @Override
    public void setChave(String chave) {
        this.chave = chave;
    }

    public String getcPFCNPJ() {
        return cPFCNPJ;
    }

    public void setcPFCNPJ(String cPFCNPJ) {
        this.cPFCNPJ = cPFCNPJ;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }
}
