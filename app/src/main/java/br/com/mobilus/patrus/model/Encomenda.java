package br.com.mobilus.patrus.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by cassio on 07/01/2016.
 */
public class Encomenda {

    @SerializedName("NumeroPedido")
    private String numeroPedido;

    @SerializedName("NumeroNotaFiscal")
    private String numeroNotaFiscal;

    @SerializedName("NumeroConhecimento")
    private String numeroConhecimento;

    @SerializedName("DescricaoStatus")
    private String descricaoStatus;

    @SerializedName("ValorServico")
    private double valorServico;

    @SerializedName("ValorFrete")
    private double valorFrete;

    @SerializedName("ValorTotal")
    private double valorTotal;

    @SerializedName("DataRegistro")
    private String dataRegistro;

    @SerializedName("DataEntrega")
    private String dataEntrega;

    @SerializedName("DataPedido")
    private String dataPedido;

    @SerializedName("Volumes")
    private double volumes;

    @SerializedName("Peso")
    private double peso;

    @SerializedName("Remetente")
    private String remetente;

    @SerializedName("Recebedor")
    private String recebedor;

    @SerializedName("Tracking")
    private List<Tracking> tracking;

    @SerializedName("Destinatario")
    private String destinatario;

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public String getDataPedido() {
        return dataPedido;
    }

    public void setDataPedido(String dataPedido) {
        this.dataPedido = dataPedido;
    }

    public List<Tracking> getTracking() {
        return tracking;
    }

    public void setTracking(List<Tracking> tracking) {
        this.tracking = tracking;
    }

    public String getDescricaoStatus() {
        return descricaoStatus;
    }

    public void setDescricaoStatus(String descricaoStatus) {
        this.descricaoStatus = descricaoStatus;
    }

    public String getRecebedor() {
        return recebedor;
    }

    public void setRecebedor(String recebedor) {
        this.recebedor = recebedor;
    }

    public String getNumeroNotaFiscal() {
        return numeroNotaFiscal;
    }

    public void setNumeroNotaFiscal(String numeroNotaFiscal) {
        this.numeroNotaFiscal = numeroNotaFiscal;
    }

    public String getNumeroConhecimento() {
        return numeroConhecimento;
    }

    public void setNumeroConhecimento(String numeroConhecimento) {
        this.numeroConhecimento = numeroConhecimento;
    }

    public double getValorServico() {
        return valorServico;
    }

    public void setValorServico(double valorServico) {
        this.valorServico = valorServico;
    }

    public double getValorFrete() {
        return valorFrete;
    }

    public void setValorFrete(double valorFrete) {
        this.valorFrete = valorFrete;
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getDataRegistro() {
        return dataRegistro;
    }

    public void setDataRegistro(String dataRegistro) {
        this.dataRegistro = dataRegistro;
    }

    public String getDataEntrega() {
        return dataEntrega;
    }

    public void setDataEntrega(String dataEntrega) {
        this.dataEntrega = dataEntrega;
    }

    public double getVolumes() {
        return volumes;
    }

    public void setVolumes(double volumes) {
        this.volumes = volumes;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public String getRemetente() {
        return remetente;
    }

    public void setRemetente(String remetente) {
        this.remetente = remetente;
    }

    public String getNumeroPedido() {
        return numeroPedido;
    }

    public void setNumeroPedido(String numeroPedido) {
        this.numeroPedido = numeroPedido;
    }
}
