package br.com.mobilus.patrus.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Locale;
import java.util.TimeZone;

public class PatrusUtil {

	public static final String PROJECT_CHAVE = "ac5e61d8c3468ff7c8dcdcadd9d99b6923228a70:f36f336fcacaeec154260a416e7ea4d39fc3f64b";
	public static final String PROJECT_GET_CHAVE = "chave=" + PROJECT_CHAVE;
	public static final String ANDROID = "ANDROID";
	// AMBIENTES
	private static final String PROJECT_SERVICE_URL_BASE_DESENV = "http://desenv.patrus.mobilus.com.br";
	private static final String PROJECT_SERVICE_URL_BASE_HOMOL = "http://homol.patrus.mobilus.com.br";
	private static final String PROJECT_SERVICE_URL_BASE_PROD = "http://mobile.patrus.com.br";

	public static String regex = "^av\\s|^avenida\\s|^av.\\s|^a\\s|^a.\\s|^rua\\s|^r\\s|^r.\\s";

	// AMBIENTE EM EXECUCAO
	public static final String URL_BASE = PROJECT_SERVICE_URL_BASE_PROD;

	public static final String PROJECT_ESQUECI_SENHA_URL = URL_BASE + "/esquecisenha";

	public static final String URL_SERVICE_BASE = URL_BASE + "/service";

	// CONFIGURACOES GERAIS
	public static final String PROJECT_DATE_SERVICE_FORMAT = "yyyy-MM-dd HH:mm:ss";

	public static final String PROJECT_DATE_WITHOUT_TIME = "yyyy-MM-dd";

	public static final String PROJECT_TIME_UI = "HH:mm";
	
	public static final String PROJECT_DATE_UI = "dd/MM/yyyy";

	public static final String PROJECT_DATE_TIME_UI = "dd/MM/yyyy 'às' HH:mm";

	public static final String PROJECT_DATE_TIME_UI_WITH_LIBE_BREAK = "dd/MM/yyyy \n'às' HH:mm";

	public static final String PROJECT_TIME_ZONE = "America/Sao_Paulo";

	public static final String PROJECT_LOCALE_LANGUAGE = "pt";

	public static final String PROJECT_LOCALE_COUNTRY = "BR";

	public static final int PROJECT_INTERVALO_SINCRONIA_EVENTO = 7;

	public static final int PROJECT_PALESTRA_CHECKIN_TRUE = 1;

	public static Locale getLocale(){
		try{
			return new Locale(PROJECT_LOCALE_LANGUAGE, PROJECT_LOCALE_COUNTRY);
		}catch(Exception e){
			return Locale.getDefault();
		}
	}

	public static TimeZone getTimeZone(){
		try{
			return TimeZone.getTimeZone(PROJECT_TIME_ZONE);
		}catch(Exception e){
			return TimeZone.getDefault();
		}
	}

	public static SimpleDateFormat getDateFormatter(){
		return new SimpleDateFormat(PROJECT_DATE_SERVICE_FORMAT, getLocale());
	}

	public static SimpleDateFormat getDateWithoutTimeFormatter(){
		return new SimpleDateFormat(PROJECT_DATE_UI, getLocale());
	}

	public static SimpleDateFormat getTimeUiFormatter(){
		return new SimpleDateFormat(PROJECT_TIME_UI, getLocale());
	}

	public static SimpleDateFormat getDateTimeUiFormatter(){
		return new SimpleDateFormat(PROJECT_DATE_TIME_UI, getLocale());
	}

	public static SimpleDateFormat getDateTimeUiFormatterWithLineBreak(){
		return new SimpleDateFormat(PROJECT_DATE_TIME_UI_WITH_LIBE_BREAK, getLocale());
	}

	public static boolean isNullOrEmpty( final Collection<?> c ) {
		return c == null || c.isEmpty();
	}

	public static boolean isNullOrEmpty(final String c) {
		return c == null || "".equals(c);
	}

	public static String doubleToCurrencyUi(double intValue){
		double doublePayment = intValue;

		DecimalFormat fmt = (DecimalFormat) NumberFormat.getInstance();
		fmt.setGroupingUsed(true);
		fmt.setPositivePrefix("R$ ");
		fmt.setNegativePrefix("R$ -");
		fmt.setMinimumFractionDigits(2);
		fmt.setMaximumFractionDigits(2);

		String s = fmt.format(doublePayment);
		return s;
	}

	public static String formatDouble(double value)
	{
		DecimalFormat format = new DecimalFormat("0.#");
		return format.format(value);
	}
}