package br.com.mobilus.patrus.service.data;

import com.google.gson.annotations.SerializedName;

import br.com.mobilus.patrus.util.PatrusUtil;

/**
 * Created by ANDROID2 on 9/8/2015.
 */
public class RequestBase {

    @SerializedName("chave")
    private String chave = PatrusUtil.PROJECT_CHAVE;

    public String getChave() {
        return chave;
    }

    public void setChave(String chave) {
        this.chave = chave;
    }
}