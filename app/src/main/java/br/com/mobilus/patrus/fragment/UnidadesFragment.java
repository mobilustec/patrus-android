package br.com.mobilus.patrus.fragment;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import br.com.mobilus.patrus.MainActivity;
import br.com.mobilus.patrus.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class UnidadesFragment extends Fragment implements OnMapReadyCallback, GoogleMap.InfoWindowAdapter
        , GoogleApiClient.ConnectionCallbacks, View.OnClickListener, GoogleApiClient.OnConnectionFailedListener, LocationListener, GoogleMap.OnInfoWindowClickListener {

    private View view;
    private MapView mMapView;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private String latitude, longitude;
    private static final String TAG = "br.com.mobilus.patrus";
    private MarkerOptions marker;
    private TextView tvNome, tvEndereco, tvEmail, tvTelefone;

    public UnidadesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_unidades, container, false);

        ((MainActivity) getActivity()).setToobarTitle("Unidades");

        tvNome = (TextView) view.findViewById(R.id.tvNome);
        tvEndereco = (TextView) view.findViewById(R.id.tvEndereco);
        tvEmail = (TextView) view.findViewById(R.id.tvEmail);
        tvTelefone = (TextView) view.findViewById(R.id.tvTelefone);

        mMapView = (MapView) view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);

        callConnection();

        return view;
    }

    private synchronized void callConnection() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addOnConnectionFailedListener(UnidadesFragment.this)
                .addConnectionCallbacks(UnidadesFragment.this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        }

        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location != null) {

            latitude = String.valueOf(location.getLatitude());
            longitude = String.valueOf(location.getLongitude());
            Log.i(TAG, "latitude: " + latitude);
            Log.i(TAG, "longitude: " + longitude);
            createMarker();
        }
    }

    @Override
    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = String.valueOf(location.getLatitude());
        longitude = String.valueOf(location.getLongitude());
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        tvNome.setText("Samuel Ribeiro");
        tvEndereco.setText("Rua Suzinha de Sá Martins, 464 - Esplanada da Estação - Itabira - MG");
        tvEmail.setText("samuel@mobilus.com.br");
        tvTelefone.setText("(31) 98977-6245");
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {

        View detalhes = getActivity().getLayoutInflater().inflate(R.layout.marker_details_image, null);
        TextView lblNomeEstabelecimento = (TextView) detalhes.findViewById(R.id.lblNomeDetalhesMarker);

        String nome = marker.getTitle().substring(0, marker.getTitle().lastIndexOf("/"));

        lblNomeEstabelecimento.setText(nome);

        return detalhes;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;

        mMap.setOnInfoWindowClickListener(this);

        double latitude = 17.385044;

        double longitude = 78.486671;

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(-14.574945, -53.773035)).zoom(3.2f).build();
        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

    }

    public void createMarker(){

        MarkerOptions marker = new MarkerOptions().position(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)));
        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_patrus));
        marker.title("Patrus");
        mMap.addMarker(marker);

    }
}
