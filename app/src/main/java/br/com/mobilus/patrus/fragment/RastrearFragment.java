package br.com.mobilus.patrus.fragment;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.roughike.bottombar.BottomBar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.mobilus.droidlib.service.ErrosV1;
import br.com.mobilus.droidlib.service.ServiceAdapterV1_2;
import br.com.mobilus.droidlib.service.retrofit.callback.CallbackV1_2;
import br.com.mobilus.patrus.MainActivity;
import br.com.mobilus.patrus.R;
import br.com.mobilus.patrus.service.Post;
import br.com.mobilus.patrus.service.data.DataRequestPedido;
import br.com.mobilus.patrus.service.data.ResponseEncomendas;
import br.com.mobilus.patrus.util.Mask;
import br.com.mobilus.patrus.util.PatrusUtil;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RastrearFragment extends Fragment implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private View view, focus;
    private BottomBar mBottomBar;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private Spinner spTipoCliente;
    private Spinner spTipoDocumento;
    private String tipoCliente, numeroDocumento, tipoDocumento, cpfCnpj;
    private EditText etCpfCnpj, etNumeroDocumento;
    private Button btnRastrear;
    private DataRequestPedido dataRequestPedido;
    private TextWatcher cpfCnpjMask, celuarMask, dataMask;


    public RastrearFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_rastrear, container, false);

        checkAndRequestPermissions();

        setInterface();

        ((MainActivity) getActivity()).setToobarTitle("Rastreie sua carga");

        return view;
    }

    private  boolean checkAndRequestPermissions() {
        int permissionCamera = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION);
        int writeExternalStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<String>();
        if (permissionCamera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (writeExternalStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    public void setInterface(){
        etCpfCnpj = (EditText) view.findViewById(R.id.etCpfCnpj);
        btnRastrear = (Button) view.findViewById(R.id.btnRastrear);
        btnRastrear.setOnClickListener(this);
        etNumeroDocumento = (EditText) view.findViewById(R.id.etNumeroDocumento);
        spTipoCliente = (Spinner) view.findViewById(R.id.spTipoCliente);
        ArrayAdapter<CharSequence> tipoClienteAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.tipo_cliente_array, android.R.layout.simple_spinner_item);
        tipoClienteAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTipoCliente.setAdapter(tipoClienteAdapter);
        spTipoCliente.setOnItemSelectedListener(this);
        spTipoDocumento = (Spinner) view.findViewById(R.id.spTipoDocumento);
        ArrayAdapter<CharSequence> tipoDocumentoAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.tipo_documento_array, android.R.layout.simple_spinner_item);
        tipoDocumentoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTipoDocumento.setAdapter(tipoDocumentoAdapter);
        spTipoDocumento.setOnItemSelectedListener(this);

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        switch (adapterView.getId()){

            case R.id.spTipoCliente:

                tipoCliente = "CPF_CNPJDestinatario";
                mascara();

                break;

            case R.id.spTipoDocumento:

                if (adapterView.getItemAtPosition(i).equals("Nota Fiscal")){
                    tipoDocumento = "NumeroNotaFiscal";
                    Toast.makeText(getActivity(), ""+tipoDocumento, Toast.LENGTH_SHORT).show();
                } else if (adapterView.getItemAtPosition(i).equals("Pedido")){
                    tipoDocumento = "NumeroPedido";
                    Toast.makeText(getActivity(), ""+tipoDocumento, Toast.LENGTH_SHORT).show();
                } else if (adapterView.getItemAtPosition(i).equals("CT-e")){
                    tipoDocumento = "NumeroConhecimento";
                    Toast.makeText(getActivity(), ""+tipoDocumento, Toast.LENGTH_SHORT).show();
                }

                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnRastrear:
                alimentaObjeto();
                break;
        }
    }

    public boolean rotinaErro() {
        if ("".equals(etCpfCnpj.getText().toString())) {
            etCpfCnpj.setError(getResources().getString(R.string.faltaCPF));
            focus = etCpfCnpj;
            return true;
        }

        if ("".equals(etNumeroDocumento.getText().toString())) {
            etNumeroDocumento.setError(getResources().getString(R.string.faltaDocumento));
            focus = etNumeroDocumento;
            return true;
        }

        return false;
    }

    private void alimentaObjeto() {

        if (!rotinaErro()){
            dataRequestPedido = new DataRequestPedido();

            dataRequestPedido.setTipoPessoa(tipoCliente);
            dataRequestPedido.setcPFCNPJ(etCpfCnpj.getText().toString());
            dataRequestPedido.setTipoDocumento(tipoDocumento);
            dataRequestPedido.setDocumento(etNumeroDocumento.getText().toString());
            serviceRastrear();
        }
    }

    public void serviceRastrear() {

        ServiceAdapterV1_2.call(PatrusUtil.URL_SERVICE_BASE, Post.class).getpedidosbycustom(dataRequestPedido, new CallbackV1_2<ResponseEncomendas>(getActivity(), R.string.aguarde, true) {
            @Override
            public void success(ResponseEncomendas responseEncomendas, Response response) {
                super.success(responseEncomendas, response);
                if (ErrosV1.NENHUM == validarResponseV1ComAlertas(getActivity(), responseEncomendas)) {
                    if (responseEncomendas.getEncomendas().size() > 0 && responseEncomendas.getEncomendas() != null){

                    }

                } else {
                    Toast.makeText(getActivity(), "Falha ao ratrear sua encomenda!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void mascara() {

        char[] tipoMascara = etCpfCnpj.getText().toString().toCharArray();

        if (tipoMascara.length <= 11){
            cpfCnpjMask = Mask.insert2("###.###.###-##", etCpfCnpj);
            etCpfCnpj.setHint(getString(R.string.digiteCPF));
            etCpfCnpj.addTextChangedListener(cpfCnpjMask);

        } else if (tipoMascara.length >= 11){
            cpfCnpjMask = Mask.insert2("##.###.###/####-##", etCpfCnpj);
            etCpfCnpj.setHint(getString(R.string.digiteCPF));
            etCpfCnpj.addTextChangedListener(cpfCnpjMask);
        }
    }
}
