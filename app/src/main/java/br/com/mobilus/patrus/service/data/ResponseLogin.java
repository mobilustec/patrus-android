package br.com.mobilus.patrus.service.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import br.com.mobilus.droidlib.service.data.response.ResponseV1;
import br.com.mobilus.patrus.model.Cliente;

/**
 * Created by ANDROID2 on 9/8/2015.
 */
public class ResponseLogin extends ResponseV1{

    @SerializedName("Cliente")
    public Cliente cliente= new Cliente();

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente associado) {
        this.cliente = associado;
    }
}