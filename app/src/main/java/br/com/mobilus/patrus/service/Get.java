package br.com.mobilus.patrus.service;


import br.com.mobilus.droidlib.service.retrofit.callback.CallbackV1_2;
import br.com.mobilus.patrus.service.data.ResponseEncomendas;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by cassio on 20/06/2015.
 */
public interface Get {

    @GET("/pedido/getpedidosemandamento")
    void getPedidosEmAndamento(
            @Query("chave") String chave,
            @Query("ClienteID") int clienteId,
            CallbackV1_2<ResponseEncomendas> cb);

    @GET("/pedido/gethistorico")
    void getHistorico(
            @Query("chave") String chave,
            @Query("ClienteID") int clienteId,
            CallbackV1_2<ResponseEncomendas> cb);
}
