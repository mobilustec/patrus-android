package br.com.mobilus.patrus.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import br.com.mobilus.droidlib.service.ErrosV1;
import br.com.mobilus.droidlib.service.ServiceAdapterV1_2;
import br.com.mobilus.droidlib.service.retrofit.callback.CallbackV1_2;
import br.com.mobilus.patrus.DetalheEncomendaActivity;
import br.com.mobilus.patrus.R;
import br.com.mobilus.patrus.adapter.EncomendaAdapter;
import br.com.mobilus.patrus.model.Encomenda;
import br.com.mobilus.patrus.service.Get;
import br.com.mobilus.patrus.service.Post;
import br.com.mobilus.patrus.service.data.DataRequestPedido;
import br.com.mobilus.patrus.service.data.ResponseEncomendas;
import br.com.mobilus.patrus.util.PatrusUtil;
import br.com.mobilus.patrus.util.Sessao;
import br.com.mobilus.patrus.util.SharedPref;
import retrofit.client.Response;

public class  EncomendasFragment extends BaseFragmentV4 {


    private ListView lvEncomendas;
    private List<Encomenda> encomendasList;
    private TextView tvTitulo;
    Context context;
    private DataRequestPedido dataRequestPedido;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize(inflater, container, savedInstanceState, R.layout.fragment_encomendas);
        lvEncomendas = (ListView) rootView.findViewById(R.id.lvHistorico);
        if (context != null) {
            servico();
        }
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void servico() {

        CallbackV1_2 cbEncomendas = new CallbackV1_2<ResponseEncomendas>(context, R.string.aguarde, true) {
            @Override
            public void success(ResponseEncomendas responseEncomendas, Response response) {
                super.success(responseEncomendas, response);
                if (ErrosV1.NENHUM == validarResponseV1ComAlertas(context, responseEncomendas)) {
                    if (responseEncomendas.getEncomendas() != null) {
                        encomendasList = responseEncomendas.getEncomendas();
                        EncomendaAdapter adapter = new EncomendaAdapter(context, encomendasList);
                        lvEncomendas.setAdapter(adapter);
                        lvEncomendas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Sessao sessao = getSessao();
                                sessao.setEncomenda(encomendasList.get(position));
                                sessao.flagDetalhe = sessao.FLAG_DETALHE_PEDIDO;
                                SharedPref.getInstance(getContext()).saveSessionState(sessao);
                                startActivity(new Intent(context, DetalheEncomendaActivity.class));
                            }
                        });
                    }
                }
            }
        };
        SharedPref sharedPref = new SharedPref(context);
        if (sharedPref != null && sharedPref.getUsuario() != null) {
            ServiceAdapterV1_2.call(PatrusUtil.URL_SERVICE_BASE, Post.class).getpedidosbycustom(dataRequestPedido, cbEncomendas);
        }
    }
}