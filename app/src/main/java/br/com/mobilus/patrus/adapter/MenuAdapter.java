package br.com.mobilus.patrus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.mobilus.patrus.R;
import br.com.mobilus.patrus.util.MenuItemPatrus;


public class MenuAdapter extends BaseAdapter{

	private static Context context;
	private LayoutInflater mLayoutInflater;
	private List<MenuItemPatrus> itens;

	public MenuAdapter(Context mContext, List<MenuItemPatrus> lista) {
		super();
		this.context = mContext;
		this.itens = lista;
		this.mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}


	@Override
	public int getCount() {
		return itens != null ? itens.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return itens != null ? itens.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		ViewHolderItem viewHolder;
		MenuItemPatrus item = itens.get(position);

		if(view == null){
			view = mLayoutInflater.inflate(R.layout.item_menu, null);
			viewHolder = new ViewHolderItem();
			viewHolder.title = (TextView) view.findViewById(R.id.tvMenuTitle);
			viewHolder.icon = (ImageView) view.findViewById(R.id.ivMenuIcon);
			view.setTag(viewHolder);
		}else{
			viewHolder = (ViewHolderItem) view.getTag();
		}



		if(item != null){
			viewHolder.title.setText(item.getMenuTitle());
			viewHolder.icon.setImageResource(item.getIconBgResource());
		}
		return view;
	}

	static class ViewHolderItem{
		TextView title;
		ImageView icon;
	}
}