package br.com.mobilus.patrus;

import android.*;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.mobilus.droidlib.service.ErrosV1;
import br.com.mobilus.droidlib.service.ServiceAdapterV1_2;
import br.com.mobilus.droidlib.service.retrofit.callback.CallbackV1_2;
import br.com.mobilus.droidlib.util.Conversor;
import br.com.mobilus.patrus.service.Post;
import br.com.mobilus.patrus.service.data.RequestLogin;
import br.com.mobilus.patrus.service.data.ResponseLogin;
import br.com.mobilus.patrus.util.PatrusUtil;
import br.com.mobilus.patrus.util.Sessao;
import br.com.mobilus.patrus.util.SharedPref;
import retrofit.client.Response;

public class LoginActivity extends ActivityBasePatrus implements Validator.ValidationListener {

    @NotEmpty(messageResId = R.string.email_obrigatorio)
    @Email(messageResId = R.string.email_invalido)
    private EditText etEmail;

    @Password(min = 6, scheme = Password.Scheme.ANY, messageResId = R.string.senha_invalida)
    private EditText etSenha;

    private Validator validator;

    private String client_id;
    private View btnLogar;
    private View btnEsqueciSenha;
    private View btnCriarConta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        pushLogin();
        validator = new Validator(this);
        validator.setValidationListener(this);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etSenha = (EditText) findViewById(R.id.etSenha);
        btnLogar = findViewById(R.id.btnLogar);
        btnLogar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });

        btnEsqueciSenha = findViewById(R.id.btnEsqueciSenha);
        btnCriarConta = findViewById(R.id.btnCriarConta);

        btnCriarConta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, CriarContaActivity.class));
            }
        });

        btnEsqueciSenha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, EsqueciSenhaActivity.class));
            }
        });

        String email = SharedPref.getInstance(getContext()).getEmail();
        etEmail.setText(email);

        Sessao sessao = SharedPref.getInstance(getContext()).restoreSessionState();
        if (sessao != null && sessao.getCliente() != null) {
            do{
                pushLogin();
            }
            while(TextUtils.isEmpty(pushLogin()));
            servicoLogar(sessao.getCliente().getEmail(), sessao.getCliente().getSenha());
        }

    }

    @Override
    public void onValidationSucceeded() {

        servicoLogar();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            }
        }
    }

    public void servicoLogar() {

        RequestLogin rl = new RequestLogin();
        rl.setEmail(etEmail.getText().toString());
        rl.setSenha(Conversor.gerarSha1(etSenha.getText().toString()));
        rl.setAmbiente(PatrusUtil.ANDROID);
        rl.setToken(client_id);

        CallbackV1_2 cbLogar = new CallbackV1_2<ResponseLogin>(getContext(), R.string.aguarde, true) {
            @Override
            public void success(ResponseLogin responseLogin, Response response) {
                super.success(responseLogin, response);
                if (ErrosV1.NENHUM == validarResponseV1ComAlertas(LoginActivity.this, responseLogin)) {
                    Sessao sessao = SharedPref.getInstance(LoginActivity.this).restoreSessionState();
                    if (responseLogin.getCliente() != null) {
                        sessao.setCliente(responseLogin.getCliente());
                        try {
                            SharedPref sharedPref = new SharedPref(LoginActivity.this);
                            sharedPref.salveUsuario(responseLogin.getCliente());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        SharedPref.getInstance(LoginActivity.this).saveSessionState(sessao);
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                    } else {
                        Toast.makeText(LoginActivity.this, "Usuario null ", Toast.LENGTH_LONG).show();

                    }
                }
            }
        };

        ServiceAdapterV1_2.call(PatrusUtil.URL_SERVICE_BASE, Post.class).logar(rl, cbLogar);
    }

    public void servicoLogar(String login, String senha) {

        RequestLogin rl = new RequestLogin();
        rl.setEmail(login);
        rl.setSenha(senha);
        rl.setAmbiente(PatrusUtil.ANDROID);
        if(!TextUtils.isEmpty(client_id)) {
            rl.setToken(client_id);
        }else {
            Toast.makeText(LoginActivity.this, "Token null ", Toast.LENGTH_LONG).show();
        }
        CallbackV1_2 cbLogar = new CallbackV1_2<ResponseLogin>(getContext(), R.string.aguarde, true) {
            @Override
            public void success(ResponseLogin responseLogin, Response response) {
                super.success(responseLogin, response);
                if (ErrosV1.NENHUM == validarResponseV1ComAlertas(LoginActivity.this, responseLogin)) {
                    Sessao sessao = SharedPref.getInstance(LoginActivity.this).restoreSessionState();
                    if (responseLogin.getCliente() != null) {
                        sessao.setCliente(responseLogin.getCliente());

                        try {
                            SharedPref sharedPref = new SharedPref(LoginActivity.this);
                            sharedPref.salveUsuario(responseLogin.getCliente());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        SharedPref.getInstance(LoginActivity.this).saveSessionState(sessao);
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                    } else {
                        Toast.makeText(LoginActivity.this, "Usuario null ", Toast.LENGTH_LONG).show();

                    }
                }
            }
        };

        ServiceAdapterV1_2.call(PatrusUtil.URL_SERVICE_BASE, Post.class).logar(rl, cbLogar);
    }

    public String pushLogin() {
        new Thread(new Runnable() {
            public void run() {
                String token = "";
                // registrar app
                InstanceID instanceID = InstanceID.getInstance(LoginActivity.this);
                try {
                    token = instanceID.getToken(
                            getString(R.string.gcm_defaultSenderIdTest),
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                client_id = token;

            }
        }).start();
        return client_id;
    }
}