package br.com.mobilus.patrus.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

import br.com.mobilus.patrus.model.Cliente;
import br.com.mobilus.patrus.util.Sessao;
import br.com.mobilus.patrus.util.SharedPref;


public class BaseFragmentV4 extends Fragment {

    protected View rootView;
    protected ViewGroup container;

    protected void initialize(LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState, int fragment) {
        rootView = inflater.inflate(fragment, container, false);
        rootView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        this.container = container;
    }

    public Cliente getCliente() {
        Sessao sessao = SharedPref.getInstance(getActivity()).restoreSessionState();
        Cliente cliente = null;
        if (sessao != null) {
            cliente = sessao.getCliente();
            if (cliente != null) {
                return cliente;
            } else {
                return cliente;
            }
        } else {
            getActivity().finish();
        }


        return cliente;
    }


    public Sessao getSessao() {
        return getSessao(getContext());
    }

    public static Sessao getSessao(Context context) {
        SharedPref pref = SharedPref.getInstance(context);
        Sessao sessao = pref.restoreSessionState();
        return sessao;
    }


}
