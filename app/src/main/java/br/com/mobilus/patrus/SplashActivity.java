package br.com.mobilus.patrus;

import android.content.Intent;
import android.os.Bundle;

import java.util.Timer;
import java.util.TimerTask;

import br.com.mobilus.patrus.util.Sessao;
import br.com.mobilus.patrus.util.SharedPref;

public class SplashActivity extends ActivityBasePatrus {

    private static final int SPLASH_TIME = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {

                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();


            }
        }, SPLASH_TIME);
    }
}