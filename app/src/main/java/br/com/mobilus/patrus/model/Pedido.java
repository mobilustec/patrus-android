package br.com.mobilus.patrus.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samuel on 25/10/16.
 */

public class Pedido {

    @SerializedName("PedidoID")
    private String pedidoID;
    @SerializedName("ClienteID")
    private Object clienteID;
    @SerializedName("FilialID")
    private String filialID;
    @SerializedName("StatusID")
    private String statusID;
    @SerializedName("NumeroNotaFiscal")
    private String numeroNotaFiscal;
    @SerializedName("NumeroPedido")
    private String numeroPedido;
    @SerializedName("NumeroConhecimento")
    private String numeroConhecimento;
    @SerializedName("Peso")
    private String peso;
    @SerializedName("Observacao")
    private Object observacao;
    @SerializedName("Origem")
    private String origem;
    @SerializedName("Destino")
    private String destino;
    @SerializedName("Volumes")
    private String volumes;
    @SerializedName("DataEntrega")
    private Object dataEntrega;
    @SerializedName("ValorServico")
    private String valorServico;
    @SerializedName("ValorFrete")
    private String valorFrete;
    @SerializedName("ValorTotal")
    private String valorTotal;
    @SerializedName("NumeroDocLogistica")
    private String numeroDocLogistica;
    @SerializedName("NotaFiscalID")
    private String notaFiscalID;
    @SerializedName("Remetente")
    private String remetente;
    @SerializedName("Recebedor")
    private String recebedor;
    @SerializedName("Parentesco")
    private Object parentesco;
    @SerializedName("DataPedido")
    private String dataPedido;
    @SerializedName("Destinatario")
    private String destinatario;
    @SerializedName("DataRegistro")
    private String dataRegistro;
    @SerializedName("CPF_CNPJDestinatario")
    private String cPFCNPJDestinatario;
    @SerializedName("Tracking")
    private List<Tracking> tracking = new ArrayList<Tracking>();

    public String getPedidoID() {
        return pedidoID;
    }

    public void setPedidoID(String pedidoID) {
        this.pedidoID = pedidoID;
    }

    public Object getClienteID() {
        return clienteID;
    }

    public void setClienteID(Object clienteID) {
        this.clienteID = clienteID;
    }

    public String getFilialID() {
        return filialID;
    }

    public void setFilialID(String filialID) {
        this.filialID = filialID;
    }

    public String getStatusID() {
        return statusID;
    }

    public void setStatusID(String statusID) {
        this.statusID = statusID;
    }

    public String getNumeroNotaFiscal() {
        return numeroNotaFiscal;
    }

    public void setNumeroNotaFiscal(String numeroNotaFiscal) {
        this.numeroNotaFiscal = numeroNotaFiscal;
    }

    public String getNumeroPedido() {
        return numeroPedido;
    }

    public void setNumeroPedido(String numeroPedido) {
        this.numeroPedido = numeroPedido;
    }

    public String getNumeroConhecimento() {
        return numeroConhecimento;
    }

    public void setNumeroConhecimento(String numeroConhecimento) {
        this.numeroConhecimento = numeroConhecimento;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public Object getObservacao() {
        return observacao;
    }

    public void setObservacao(Object observacao) {
        this.observacao = observacao;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getVolumes() {
        return volumes;
    }

    public void setVolumes(String volumes) {
        this.volumes = volumes;
    }

    public Object getDataEntrega() {
        return dataEntrega;
    }

    public void setDataEntrega(Object dataEntrega) {
        this.dataEntrega = dataEntrega;
    }

    public String getValorServico() {
        return valorServico;
    }

    public void setValorServico(String valorServico) {
        this.valorServico = valorServico;
    }

    public String getValorFrete() {
        return valorFrete;
    }

    public void setValorFrete(String valorFrete) {
        this.valorFrete = valorFrete;
    }

    public String getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(String valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getNumeroDocLogistica() {
        return numeroDocLogistica;
    }

    public void setNumeroDocLogistica(String numeroDocLogistica) {
        this.numeroDocLogistica = numeroDocLogistica;
    }

    public String getNotaFiscalID() {
        return notaFiscalID;
    }

    public void setNotaFiscalID(String notaFiscalID) {
        this.notaFiscalID = notaFiscalID;
    }

    public String getRemetente() {
        return remetente;
    }

    public void setRemetente(String remetente) {
        this.remetente = remetente;
    }

    public String getRecebedor() {
        return recebedor;
    }

    public void setRecebedor(String recebedor) {
        this.recebedor = recebedor;
    }

    public Object getParentesco() {
        return parentesco;
    }

    public void setParentesco(Object parentesco) {
        this.parentesco = parentesco;
    }

    public String getDataPedido() {
        return dataPedido;
    }

    public void setDataPedido(String dataPedido) {
        this.dataPedido = dataPedido;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public String getDataRegistro() {
        return dataRegistro;
    }

    public void setDataRegistro(String dataRegistro) {
        this.dataRegistro = dataRegistro;
    }

    public String getcPFCNPJDestinatario() {
        return cPFCNPJDestinatario;
    }

    public void setcPFCNPJDestinatario(String cPFCNPJDestinatario) {
        this.cPFCNPJDestinatario = cPFCNPJDestinatario;
    }

    public List<Tracking> getTracking() {
        return tracking;
    }

    public void setTracking(List<Tracking> tracking) {
        this.tracking = tracking;
    }
}
