package br.com.mobilus.patrus.service.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ANDROID2 on 9/8/2015.
 */
public class RequestContato extends RequestBase {


    @SerializedName("Nome")
    private String nome;

    @SerializedName("Email")
    private String email;

    @SerializedName("Assunto")
    private String assunto;

    @SerializedName("Mensagem")
    private String mensagem;

    @SerializedName("ClienteID")
    private int clienteID;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAssunto() {
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public int getClienteID() {
        return clienteID;
    }

    public void setClienteID(int clienteID) {
        this.clienteID = clienteID;
    }
}