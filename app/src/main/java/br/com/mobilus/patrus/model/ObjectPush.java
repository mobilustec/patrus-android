package br.com.mobilus.patrus.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinicius on 29/03/2016.
 */
public class ObjectPush {
    @SerializedName("TipoMensagem")
    public String tipoMensagem = "";
    @SerializedName("Servico")
    public String servico = "";
    @SerializedName("Badges")
    public int badges=1;
    public String sound = "";
    @SerializedName("Mensagem")
    public String mensagem = "";
    @SerializedName("PedidoID")
    private String pedidoId = "";
    @SerializedName("PropostaID")
    private String propostaId = "";
    @SerializedName("Tipo")
    private String tipo = "";
    @SerializedName("Usuario")
    private String usuario;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getPedidoId() {
        return pedidoId;
    }

    public void setPedidoId(String pedidoId) {
        this.pedidoId = pedidoId;
    }

    public String getPropostaId() {
        return propostaId;
    }

    public void setPropostaId(String propostaId) {
        this.propostaId = propostaId;
    }

    public String getTipoMensagem() {
        return tipoMensagem;
    }

    public void setTipoMensagem(String tipoMensagem) {
        this.tipoMensagem = tipoMensagem;
    }

    public String getServico() {
        return servico;
    }

    public void setServico(String servico) {
        this.servico = servico;
    }

    public int getBadges() {
        return badges;
    }

    public void setBadges(int badges) {
        this.badges = badges;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
