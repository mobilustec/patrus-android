package br.com.mobilus.patrus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.mobilus.patrus.R;
import br.com.mobilus.patrus.model.Encomenda;
import br.com.mobilus.patrus.util.PatrusUtil;


public class EncomendaAdapter extends BaseAdapter{

	private static Context context;
	private LayoutInflater mLayoutInflater;
	private List<Encomenda> itens;

	public EncomendaAdapter(Context mContext, List<Encomenda> lista) {
		super();
		this.context = mContext;
		this.itens = lista;
		this.mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}


	@Override
	public int getCount() {
		return itens != null ? itens.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return itens != null ? itens.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		ViewHolderItem viewHolder;
		Encomenda item = (Encomenda)itens.get(position);

		if(view == null){
			view = mLayoutInflater.inflate(R.layout.item_lista_pedido, null);
			viewHolder = new ViewHolderItem();
			viewHolder.tvDescricao = (TextView) view.findViewById(R.id.tvDescricao);
			viewHolder.tvQuantidade = (TextView) view.findViewById(R.id.tvQuantidade);
			viewHolder.tvValor = (TextView) view.findViewById(R.id.tvValor);
			viewHolder.tvStatus = (TextView) view.findViewById(R.id.tvStatus);
			view.setTag(viewHolder);
		}else{
			viewHolder = (ViewHolderItem) view.getTag();
		}

		if(item != null){

			if(item.getRemetente() != null)
			viewHolder.tvDescricao.setText(item.getRemetente());

			if(item.getDescricaoStatus() != null)
				viewHolder.tvStatus.setText(item.getDescricaoStatus());

			String volumes = PatrusUtil.formatDouble(item.getVolumes()) + " ";

			volumes = volumes + context.getResources().getString(R.string.volumes);

			viewHolder.tvQuantidade.setText(volumes);

			viewHolder.tvValor.setText(PatrusUtil.doubleToCurrencyUi((item.getValorTotal())));
		}
		return view;
	}

	static class ViewHolderItem{
		TextView tvDescricao;
		TextView tvQuantidade;
		TextView tvValor;
		TextView tvStatus;
	}
}