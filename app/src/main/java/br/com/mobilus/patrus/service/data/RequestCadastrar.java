package br.com.mobilus.patrus.service.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ANDROID2 on 9/8/2015.
 */
public class RequestCadastrar extends RequestBase {


    @SerializedName("Nome")
    private String nome;

    @SerializedName("Email")
    private String email;

    @SerializedName("Senha")
    private String senha;

    @SerializedName("Telefone")
    private String telefone;

    @SerializedName("CPF_CNPJ")
    private String cpfCnpj;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }
}