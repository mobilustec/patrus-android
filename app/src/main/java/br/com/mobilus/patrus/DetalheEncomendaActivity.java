package br.com.mobilus.patrus;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import br.com.mobilus.patrus.adapter.TrackingAdapter;
import br.com.mobilus.patrus.model.Encomenda;
import br.com.mobilus.patrus.util.PatrusUtil;

public class DetalheEncomendaActivity extends ActivityBasePatrus {

    private TextView tvPedido;
    private TextView tvNf;
    private TextView tvNumeroConhecimento;
    private TextView tvRemetente;
    private TextView tvQuantidade;
    private TextView tvValor;
    private TextView tvRecebedor;
    private ListView lvTracking;
    private TextView tvNenhumRegistro;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_encomenda);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundResource(R.drawable.nav_bar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tvPedido = (TextView) findViewById(R.id.tvPedido);
        tvNf = (TextView) findViewById(R.id.tvNf);
        tvNumeroConhecimento = (TextView) findViewById(R.id.tvNumeroConhecimento);
        tvRemetente = (TextView) findViewById(R.id.tvRemetente);
        tvRecebedor = (TextView) findViewById(R.id.tvRecebedor);
        tvQuantidade = (TextView) findViewById(R.id.tvQuantidade);
        tvValor = (TextView) findViewById(R.id.tvValor);
        lvTracking = (ListView) findViewById(R.id.lvTracking);
        tvNenhumRegistro = (TextView) findViewById(R.id.tvNenhumRegistro);

        Encomenda encomenda = getSessao().getEncomenda();

//        if(getSessao().flagDetalhe == getSessao().FLAG_DETALHE_PEDIDO)
//        {
//            tvTitulo.setText(getResources().getString(R.string.encomendas));
//        }else{
//            tvTitulo.setText(getResources().getString(R.string.historico));
//        }

        if(encomenda != null) {

            if(encomenda.getTracking() != null && !encomenda.getTracking().isEmpty())
            {
                TrackingAdapter adapter = new TrackingAdapter(getContext(), encomenda.getTracking());
                lvTracking.setAdapter(adapter);
                lvTracking.setVisibility(View.VISIBLE);
                tvNenhumRegistro.setVisibility(View.GONE);
            }else{
                lvTracking.setVisibility(View.GONE);
                tvNenhumRegistro.setVisibility(View.VISIBLE);
            }

            if (encomenda.getNumeroPedido() != null)
                tvPedido.setText(encomenda.getNumeroPedido());

            if(encomenda.getDestinatario() != null) {
                tvRemetente.setText(encomenda.getDestinatario());
            }

            if(encomenda.getRecebedor() != null){
                tvRecebedor.setText(encomenda.getRecebedor());
            }

            if(encomenda.getNumeroNotaFiscal() != null)
                tvNf.setText(encomenda.getNumeroNotaFiscal());

            if(encomenda.getNumeroConhecimento() != null)
                tvNumeroConhecimento.setText(encomenda.getNumeroConhecimento());

            String volumes = PatrusUtil.formatDouble(encomenda.getVolumes()) + " ";
            if(encomenda.getVolumes() > 1) {
                volumes = volumes + "Volumes";
            }else{
                volumes = volumes + "Volume";
            }

            tvQuantidade.setText(volumes);

            tvValor.setText(PatrusUtil.doubleToCurrencyUi((encomenda.getValorTotal())));
        }
    }
}