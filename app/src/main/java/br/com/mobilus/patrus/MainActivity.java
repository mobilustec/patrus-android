package br.com.mobilus.patrus;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import br.com.mobilus.patrus.fragment.ContatoFragment;
import br.com.mobilus.patrus.fragment.EncomendasFragment;
import br.com.mobilus.patrus.fragment.RastrearFragment;
import br.com.mobilus.patrus.fragment.UnidadesFragment;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class MainActivity extends ActivityBasePatrus {

    private static final int MENU_LISTA_ENCOMENDAS = 0;
    private static final int MENU_HISTORICO = 1;
    private static final int MENU_MEUS_CONTATO = 2;
    private static final int MENU_SAIR = 3;
    public static int REQUEST_LOGAR = 171;
    public static boolean refreshMenu = false;
    private static int MENU_ULTIMO = MENU_LISTA_ENCOMENDAS;
    public FragmentManager fragmentManager;
    Fragment fragmentToLoad;
    private DrawerLayout layoutDrawer;
    private LinearLayout linearDrawer;
    private Toolbar toolbar;
    private TextView tvToolbarTitle;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        linearDrawer = (LinearLayout) findViewById(R.id.linearDrawer);
//        layoutDrawer = (DrawerLayout) findViewById(R.id.layoutDrawer);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundResource(R.drawable.nav_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tvToolbarTitle = (TextView) findViewById(R.id.toolbar_title);

        fragmentManager = getSupportFragmentManager();

        BottomBar bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                if (tabId == R.id.tab_consulta) {
                    RastrearFragment rastrearFragment = new RastrearFragment();
                    fragmentManager.beginTransaction().replace(R.id.flContent, rastrearFragment).addToBackStack("rastrearFragment").commit();
                }
                if (tabId == R.id.tab_unidades) {
                    UnidadesFragment unidadesFragment = new UnidadesFragment();
                    fragmentManager.beginTransaction().replace(R.id.flContent, unidadesFragment).addToBackStack("unidadesmap").commit();
                }
                if (tabId == R.id.tab_encomendas) {
                    EncomendasFragment encomendasFragment = new EncomendasFragment();
                    fragmentManager.beginTransaction().replace(R.id.flContent, encomendasFragment).addToBackStack("encomendas").commit();
                }
                if (tabId == R.id.tab_contato) {
                    ContatoFragment contatoFragment = new ContatoFragment();
                    fragmentManager.beginTransaction().replace(R.id.flContent, contatoFragment).addToBackStack("contato").commit();
                }
            }
        });

        loadFragmentHome();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int res_id = item.getItemId();
        switch (res_id){
            case R.id.action_login:
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
        }
        return true;
    }

    private void loadFragmentHome() {

        Fragment fragment = null;

        Class fragmentClass;

        fragmentClass = RastrearFragment.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack("principal").commit();

    }

    public void refreshFragment() {
        fragmentManager = getSupportFragmentManager();
        fragmentToLoad = new EncomendasFragment();
        getSupportActionBar().setTitle(getResources().getString(R.string.menuEstacionar));
//        fragmentManager.beginTransaction().replace(R.id.content_frame, fragmentToLoad).commit();
//        layoutDrawer.closeDrawer(linearDrawer);
    }

    public void setToobarTitle(String title) {
        tvToolbarTitle.setText(title);
    }

}