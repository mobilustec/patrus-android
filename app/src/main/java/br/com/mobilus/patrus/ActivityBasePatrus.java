package br.com.mobilus.patrus;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;

import br.com.mobilus.patrus.model.Cliente;
import br.com.mobilus.patrus.util.AppIcon;
import br.com.mobilus.patrus.util.Sessao;
import br.com.mobilus.patrus.util.SharedPref;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by ANDROID2 on 9/9/2015.
 */
public class ActivityBasePatrus extends AppCompatActivity {

    public ActionBar customActionBar;
    public TextView actionBarTitle;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private static Sessao dadosSessao;


    public static void setSessao(Sessao sessao) {
        dadosSessao = sessao;
    }

    public Sessao getSessao() {
        return getSessao(this);
    }

    public static Sessao getSessao(Context context) {
        SharedPref pref = SharedPref.getInstance(context);
        setSessao(pref.restoreSessionState());
        return dadosSessao;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        SharedPref pref = SharedPref.getInstance(this);
        Log.d("MEMORIA TESTE", "SALVANDO INSTANCIA");
        pref.saveSessionState(getSessao(this));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setDisplayShowHomeEnabled(true);
//        actionBar.setHomeButtonEnabled(true);
//        actionBar.setDisplayHomeAsUpEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

//        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//        getSupportActionBar().setCustomView(R.layout.action_bar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);
//        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.act_bar));
        AppIcon.clearBadge(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            setResult(RESULT_CANCELED);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public static boolean podeEstacionar(long tempoRestante) {
        if (tempoRestante == -1) {
            return true;
        }
        return tempoRestante > 0;
    }

    protected AppCompatActivity getActivity() {
        return this;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected Context getContext() {
        return ActivityBasePatrus.this;
    }

    public Cliente getCliente() {
        Sessao sessao = SharedPref.getInstance(getContext()).restoreSessionState();
        if (sessao == null)
            getActivity().finish();

        Cliente cliente = sessao.getCliente();
        if (cliente == null)
            getActivity().finish();

        return cliente;
    }

}