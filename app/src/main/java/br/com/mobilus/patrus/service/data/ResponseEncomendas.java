package br.com.mobilus.patrus.service.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import br.com.mobilus.droidlib.service.data.response.ResponseV1;
import br.com.mobilus.patrus.model.Encomenda;

/**
 * Created by ANDROID2 on 9/8/2015.
 */
public class ResponseEncomendas extends ResponseV1{

    @SerializedName("Pedidos")
    private List<Encomenda> encomendas;

    public List<Encomenda> getEncomendas() {
        return encomendas;
    }

    public void setEncomendas(List<Encomenda> encomendas) {
        this.encomendas = encomendas;
    }
}