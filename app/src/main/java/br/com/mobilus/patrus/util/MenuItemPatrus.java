package br.com.mobilus.patrus.util;

public class MenuItemPatrus {

	private String menuTitle;
	private int id;
	private int iconBgResource;

	public MenuItemPatrus(int id, String title, int ic_menu_home){
		this.id = id;
		this.menuTitle = title;
		this.iconBgResource = ic_menu_home;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMenuTitle() {
		return menuTitle;
	}

	public void setMenuTitle(String menuTitle) {
		menuTitle = menuTitle;
	}

	public int getIconBgResource() {
		return iconBgResource;
	}

	public void setIconBgResource(int iconBgResource){
		this.iconBgResource = iconBgResource;
	}
}
