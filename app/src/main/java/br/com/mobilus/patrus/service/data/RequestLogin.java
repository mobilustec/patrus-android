package br.com.mobilus.patrus.service.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ANDROID2 on 9/8/2015.
 */
public class RequestLogin extends RequestBase {

    public static final int ROTULO_BLUEPARK = 1;
    public static final int ROTULO_FACEBOOK = 2;

    @SerializedName("Nome")
    private String nome;

    @SerializedName("Email")
    private String email;

    @SerializedName("Senha")
    private String senha;

    @SerializedName("URLFoto")
    private String urlFoto;

    @SerializedName("RotuloID")
    private int rotuloId;
    @SerializedName("PushToken")
    private String token;
    @SerializedName("Ambiente")
    private String ambiente;

    @SerializedName("DataAtualizacao")
    private String dataAtualizacao;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

    public String getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(String dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public int getRotuloId() {
        return rotuloId;
    }

    public void setRotuloId(int rotuloId) {
        this.rotuloId = rotuloId;
    }
}
