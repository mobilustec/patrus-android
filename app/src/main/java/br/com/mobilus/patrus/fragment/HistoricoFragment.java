package br.com.mobilus.patrus.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import br.com.mobilus.droidlib.service.ErrosV1;
import br.com.mobilus.droidlib.service.ServiceAdapterV1_2;
import br.com.mobilus.droidlib.service.retrofit.callback.CallbackV1_2;
import br.com.mobilus.patrus.DetalheEncomendaActivity;
import br.com.mobilus.patrus.MainActivity;
import br.com.mobilus.patrus.R;
import br.com.mobilus.patrus.adapter.HistoricoAdapter;
import br.com.mobilus.patrus.model.Encomenda;
import br.com.mobilus.patrus.service.Get;
import br.com.mobilus.patrus.service.data.ResponseEncomendas;
import br.com.mobilus.patrus.util.PatrusUtil;
import br.com.mobilus.patrus.util.Sessao;
import br.com.mobilus.patrus.util.SharedPref;
import retrofit.client.Response;

public class HistoricoFragment extends BaseFragmentV4 {


	private ListView lvHistorico;
	private List<Encomenda> historicoList;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initialize(inflater, container, savedInstanceState, R.layout.fragment_encomendas);
		lvHistorico = (ListView) rootView.findViewById(R.id.lvHistorico);
        ((MainActivity) getActivity()).setToobarTitle("Encomendas");
		servico();
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	public void servico(){

		CallbackV1_2 cbEncomendas = new CallbackV1_2<ResponseEncomendas>(getContext(), R.string.aguarde, true){
			@Override
			public void success(ResponseEncomendas responseEncomendas, Response response) {
				super.success(responseEncomendas, response);
				if(ErrosV1.NENHUM == validarResponseV1ComAlertas(getContext(), responseEncomendas))
				{
					if(responseEncomendas.getEncomendas() != null) {
						historicoList = responseEncomendas.getEncomendas();
						HistoricoAdapter adapter = new HistoricoAdapter(getContext(), historicoList);
						lvHistorico.setAdapter(adapter);
						lvHistorico.setOnItemClickListener(new AdapterView.OnItemClickListener() {
							@Override
							public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
								Sessao sessao = getSessao();
								sessao.setEncomenda(historicoList.get(position));
								sessao.flagDetalhe = sessao.FLAG_DETALHE_HISTORICO;
								SharedPref.getInstance(getContext()).saveSessionState(sessao);
								startActivity(new Intent(getActivity(), DetalheEncomendaActivity.class));

							}
						});
					}
				}
			}
		};
		SharedPref sharedPref = new SharedPref(getActivity());
		if (sharedPref!= null && sharedPref.getUsuario() != null) {
			ServiceAdapterV1_2.call(PatrusUtil.URL_SERVICE_BASE, Get.class).getHistorico(PatrusUtil.PROJECT_CHAVE,sharedPref.getUsuario().getClienteId(), cbEncomendas);
		}
	}
}