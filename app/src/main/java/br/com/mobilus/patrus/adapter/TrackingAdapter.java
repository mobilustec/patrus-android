package br.com.mobilus.patrus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import br.com.mobilus.patrus.R;
import br.com.mobilus.patrus.model.Tracking;
import br.com.mobilus.patrus.util.PatrusUtil;


public class TrackingAdapter extends BaseAdapter{

	private static Context context;
	private LayoutInflater mLayoutInflater;
	private List<Tracking> itens;

	public TrackingAdapter(Context mContext, List<Tracking> lista) {
		super();
		this.context = mContext;
		this.itens = lista;
		this.mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}


	@Override
	public int getCount() {
		return itens != null ? itens.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return itens != null ? itens.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		ViewHolderItem viewHolder;
		Tracking item = (Tracking)itens.get(position);

		if(view == null){
			view = mLayoutInflater.inflate(R.layout.item_lista_tracking, null);
			viewHolder = new ViewHolderItem();
			viewHolder.tvData = (TextView) view.findViewById(R.id.tvData);
			viewHolder.tvStatus = (TextView) view.findViewById(R.id.tvStatus);
			viewHolder.ivIcone = (ImageView) view.findViewById(R.id.ivIcone);
			view.setTag(viewHolder);
		}else{
			viewHolder = (ViewHolderItem) view.getTag();
		}

		if(item != null) {
			if (item.getData() != null) {
				try {
					Date data = PatrusUtil.getDateFormatter().parse(item.getData());
					viewHolder.tvData.setText(PatrusUtil.getDateTimeUiFormatter().format(data));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			if (item.getStatusTracking() != null) {
				viewHolder.tvStatus.setText(item.getStatusTracking());
			}

			Glide.with(context).load(item.getURLImagem()).into(viewHolder.ivIcone);

		}
		return view;
	}

	static class ViewHolderItem{
		TextView tvData;
		TextView tvStatus;
		ImageView ivIcone;
	}
}