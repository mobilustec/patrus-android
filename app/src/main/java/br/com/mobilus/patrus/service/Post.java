package br.com.mobilus.patrus.service;


import br.com.mobilus.droidlib.service.data.response.ResponseV1;
import br.com.mobilus.droidlib.service.retrofit.callback.CallbackV1_2;
import br.com.mobilus.patrus.service.data.DataRequestPedido;
import br.com.mobilus.patrus.service.data.RequestCadastrar;
import br.com.mobilus.patrus.service.data.RequestContato;
import br.com.mobilus.patrus.service.data.RequestLogin;
import br.com.mobilus.patrus.service.data.ResponseEncomendas;
import br.com.mobilus.patrus.service.data.ResponseLogin;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by cassio on 20/06/2015.
 */
public interface Post {

    @POST("/cliente/logar")
    void logar(@Body RequestLogin cliente, Callback<ResponseLogin> cb);

    @POST("/cliente/cadastrar")
    void cadastrar(@Body RequestCadastrar cadastrar, Callback<ResponseV1> cb);

    @POST("/contato/mensagem")
    void contato(@Body RequestContato contato, Callback<ResponseV1> cb);

    @POST("/pedido/getpedidosbycustom")
    void getpedidosbycustom(@Body DataRequestPedido dataRequestPedido, CallbackV1_2<ResponseEncomendas> cb);
}