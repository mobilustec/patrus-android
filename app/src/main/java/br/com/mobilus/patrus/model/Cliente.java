package br.com.mobilus.patrus.model;

import com.google.gson.annotations.SerializedName;

public class Cliente {


	@SerializedName("ClienteID")
	private int clienteId;

	@SerializedName("StatusID")
	private int statusId;

	@SerializedName("CPF_CNPJ")
	private String cpfCnpj;

	@SerializedName("Nome")
	private String nomej;

	@SerializedName("Email")
	private String email;

	@SerializedName("Senha")
	private String senha;

	@SerializedName("Telefone")
	private String telefone;

	public int getClienteId() {
		return clienteId;
	}

	public void setClienteId(int clienteId) {
		this.clienteId = clienteId;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getNomej() {
		return nomej;
	}

	public void setNomej(String nomej) {
		this.nomej = nomej;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
}