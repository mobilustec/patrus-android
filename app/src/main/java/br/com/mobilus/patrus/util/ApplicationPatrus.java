package br.com.mobilus.patrus.util;

import android.app.Application;

import br.com.mobilus.patrus.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by ANDROID2 on 9/9/2015.
 */
public class ApplicationPatrus extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/Roboto-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
    }
}
