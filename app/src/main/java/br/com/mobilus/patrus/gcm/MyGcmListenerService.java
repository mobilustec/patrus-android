package br.com.mobilus.patrus.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;

import com.google.android.gms.gcm.GcmListenerService;

import br.com.mobilus.droidlib.util.DataSerializer;
import br.com.mobilus.patrus.LoginActivity;
import br.com.mobilus.patrus.R;
import br.com.mobilus.patrus.model.ObjectPush;
import br.com.mobilus.patrus.util.AppIcon;


public class MyGcmListenerService extends GcmListenerService {
    // Pushtoken pushtoken;
    // A integer, that identifies each notification uniquely
    public static final int NOTIFICATION_ID = 1;
    private static final String CLIENTE = "C";
    private static final String PROFISSIONAL = "P";
    private static final String PROPOSTA = "Proposta";

    NotificationManager notificationManager;

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        super.onMessageReceived(from, data);
        String message = data.getString("dados");
        ObjectPush objectPush = DataSerializer.getInstance().toObject(message, ObjectPush.class);
        notificacao(objectPush.getMensagem(), objectPush.getBadges());

    }

    public void notificacao(String mensagem, int count) {

        // atende ate 2.3 android suport
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        AppIcon.setBadgeFull(this, count);
        //icon da notificacao a direita
        builder.setSmallIcon(R.drawable.ic_notification);
        //builder.setLargeIcon(HijobUtil.drawableToBitmap(getDrawable(R.drawable.ic_launcher)));

        builder.setAutoCancel(true);

        //intent para chamar a tela desejada
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);

        //setando intent para chamar a tela desejada
        builder.setContentIntent(pendingIntent);

        //grande icon a esquerdasss
        //  builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher));

        //mensagem de titulo ararar
        builder.setContentTitle(getString(R.string.app_name));

        //texto de conteudo da notificacao
        builder.setContentText(mensagem);
        // vibracao
        builder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});

        //Uri sound = Uri.parse("android.resource://" + getPackageName() + "/" + );
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(alarmSound);

        //esse so subtexto so aparece para os dispositivos mais novos
        // builder.setSubText(mensagem);

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        //as coisas acontecem aqui kkk
        notificationManager.notify(NOTIFICATION_ID, builder.build());

    }
  /*  private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.im_logo : R.drawable.ic_launcher;
    }*/
   /* public void notificacaoPerfil(String mensagem, Usuario usuario) {
        String mensagemNotificao = mensagem;
        String titulo = "Votemix";

        // atende ate 2.3 android suport
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        //icon da notificacao a direita
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setAutoCancel(true);

        //intent para chamar o perfil festa


        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), ActivityPerfilUsuario.class);
        intent.putExtra("idUsuario", String.valueOf(usuario.getUsuarioId()));
        intent.putExtra("idArtista", String.valueOf(usuario.getUsuarioId()));
        intent.putExtra("nome", usuario.getNome().toString());
        intent.putExtra("emailUsuario", usuario.getEmail().toString());
        intent.putExtra("tipoUsuario", usuario.getTipoUsuarioID().toString());

        try {
            intent.putExtra("usuarioSerialized", DataSerializer.getInstance().toJson(usuario));
        } catch (IOException e) {
            e.printStackTrace();
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 5, intent, 0);

        // quando clicado na notificacao essa intent chamara o perfil festa
        builder.setContentIntent(pendingIntent);

        //grande icon a esquerdasss
        //  builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher));

        //mensagem de titulo ararar
        builder.setContentTitle("Votemix");

        //texto de conteudo da notificacao
        builder.setContentText(mensagemNotificao);
        //vibracao
        // builder.setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });
        //queria colocar o sound custom mais o r ta zuando comigo
        //Uri sound = Uri.parse("android.resource://" + getPackageName() + "/" + );
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(alarmSound);

        //esse so subtexto so aparece para os dispositivos mais novos
        builder.setSubText(mensagemNotificao);

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        //as coisas acontecem aqui kkk
        notificationManager.notify(NOTIFICATION_ID, builder.build());

    }*/


}
