package br.com.mobilus.patrus.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cassio on 07/01/2016.
 */
public class Tracking {

    public static final int STATUS_DOCUMENTO_EMITIDO = 1;
    public static final int STATUS_EM_TRANSFERENCIA = 2;
    public static final int STATUS_CHEGADA_FILIAL = 3;
    public static final int STATUS_SAIU_PARA_ENTREGA = 4;
    public static final int STATUS_ENTREGA_REALIZADA = 5;
    public static final int STATUS_RECUSADO = 6;

    @SerializedName("TrackingID")
    private int trackingId;

    @SerializedName("PedidoID")
    private int pedidoID;

    @SerializedName("StatusID")
    private int statusId;

    @SerializedName("Data")
    private String data;

    @SerializedName("StatusTracking")
    private String statusTracking;

    @SerializedName("URLImagem")
    private String URLImagem;

    public String getURLImagem() {
        return URLImagem;
    }

    public void setURLImagem(String URLImagem) {
        this.URLImagem = URLImagem;
    }

    public String getStatusTracking() {
        return statusTracking;
    }

    public void setStatusTracking(String statusTracking) {
        this.statusTracking = statusTracking;
    }

    public int getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(int trackingId) {
        this.trackingId = trackingId;
    }

    public int getPedidoID() {
        return pedidoID;
    }

    public void setPedidoID(int pedidoID) {
        this.pedidoID = pedidoID;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
