package br.com.mobilus.patrus;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import br.com.mobilus.droidlib.service.ErrosV1;
import br.com.mobilus.droidlib.service.ServiceAdapterV1_2;
import br.com.mobilus.droidlib.service.data.response.ResponseV1;
import br.com.mobilus.droidlib.service.retrofit.callback.CallbackV1_2;
import br.com.mobilus.droidlib.util.AlertasV1;
import br.com.mobilus.droidlib.util.Conversor;
import br.com.mobilus.patrus.service.Post;
import br.com.mobilus.patrus.service.data.RequestCadastrar;
import br.com.mobilus.patrus.util.Mask;
import br.com.mobilus.patrus.util.PatrusUtil;
import br.com.mobilus.patrus.util.Validador;
import br.com.mobilus.patrus.util.ValidadorDigitos;
import retrofit.client.Response;

public class CriarContaActivity extends ActivityBasePatrus implements Validator.ValidationListener{


    private EditText etCpf;

    @Password(min = 6, messageResId = R.string.senha_minimo_digitos, scheme = Password.Scheme.ANY)
    private EditText etSenha;

    @ConfirmPassword(messageResId = R.string.confirmacao_senha_invalida)
    private EditText etConfirmarSenha;

    @Email(messageResId = R.string.email_invalido)
    private EditText etEmail;

    @NotEmpty(messageResId = R.string.campo_obrigatorio)
    private EditText etNome;

    @NotEmpty(messageResId = R.string.campo_obrigatorio)
//    private EditText etTelefone;

    private TextWatcher cpfMask;
//    private TextWatcher telefoneMask;

    private Validator validator;

    private Toolbar toolbar;

    private TextView tvToolbarTitle;

//    private Button btnCriarConta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_criar_conta);
        validator = new Validator(this);
        validator.setValidationListener(this);

        getWidgets();

        mascaraCpf();

//        btnCriarConta.setOnClickListener(new View.OnClickListener() {
//            @Override
//                public void onClick(View v) {
//
//                int tamanho = etCpf.getText().toString().length();
//                if(tamanho > 15)
//                {
//                    if(!Validador.isCnpjValido(etCpf.getText().toString())){
//                        AlertasV1.alerta(getContext(), R.string.digite_cnpj_valido);
//                        return;
//                    }
//                }else{
//
//                    if(!ValidadorDigitos.isCPF(etCpf.getText().toString())){
//                        AlertasV1.alerta(getContext(), R.string.digite_cpf_valido);
//                        return;
//                    }
//                }
//                validator.validate();
//            }
//        });
    }

    public void getWidgets(){
        etNome = (EditText) findViewById(R.id.etNome);
        etEmail = (EditText) findViewById(R.id.etEmail);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundResource(R.drawable.nav_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tvToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        setToobarTitle("Cadastro");
//        etTelefone = (EditText) findViewById(R.id.etTelefone);
        etCpf = (EditText) findViewById(R.id.etCpf);
        etSenha = (EditText) findViewById(R.id.etSenha);
        etConfirmarSenha = (EditText) findViewById(R.id.etConfirmarSenha);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_cadastro, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int res_id = item.getItemId();
        switch (res_id){
            case R.id.action_salvar:
                int tamanho = etCpf.getText().toString().length();
                if(tamanho > 15)
                {
                    if(!Validador.isCnpjValido(etCpf.getText().toString())){
                        AlertasV1.alerta(getContext(), R.string.digite_cnpj_valido);
                        return true;
                    }
                }else{

                    if(!ValidadorDigitos.isCPF(etCpf.getText().toString())){
                        AlertasV1.alerta(getContext(), R.string.digite_cpf_valido);
                        return true;
                    }
                }
                validator.validate();        }
        return true;
    }

    @Override
    public void onValidationSucceeded() {
        servicoCadastrar();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            }
        }
    }

    public void servicoCadastrar(){

        RequestCadastrar rc = new RequestCadastrar();
        rc.setSenha(Conversor.gerarSha1(etSenha.getText().toString()));
        rc.setEmail(etEmail.getText().toString());
        rc.setCpfCnpj(etCpf.getText().toString());
        rc.setNome(etNome.getText().toString());
//        rc.setTelefone(etTelefone.getText().toString());

        CallbackV1_2 cbCadastrar = new CallbackV1_2<ResponseV1>(getContext(), R.string.aguarde, true){
            @Override
            public void success(ResponseV1 responseV1, Response response) {
                super.success(responseV1, response);
                if(ErrosV1.NENHUM == validarResponseV1ComAlertas(getContext(), responseV1)){
                    AlertasV1.alerta(getContext(), R.string.cadastrado_com_sucesso).setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            finish();
                        }
                    });
                }
            }
        };

        ServiceAdapterV1_2.call(PatrusUtil.URL_SERVICE_BASE, Post.class).cadastrar(rc, cbCadastrar);

    }

    public void mascaraCpf() {
        cpfMask = Mask.insert2("###.###.###-##", etCpf);
        etCpf.addTextChangedListener(cpfMask);
//        telefoneMask = Mask.insert("(##)####-#####", etTelefone);
//        etTelefone.addTextChangedListener(telefoneMask);
    }

    public void setToobarTitle(String title) {
        tvToolbarTitle.setText(title);
    }
}